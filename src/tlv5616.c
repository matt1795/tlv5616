// TLV5616 Linux Kernel Driver
//
// Author: Matthew Knight
// File Name: tlv5616.c
// Date: 2019-11-25
//
// The TLV5616 is a single channel, 12-bit DAC. This device driver creates
// a character device that allows a user to write a value to the device by
// writing a 16-bit word to a file.

#include <linux/device.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/spi/spi.h>
#include <linux/spinlock.h>
#include <linux/uaccess.h>

#define DEVICE_NAME "tlv5616"
#define CLASS_NAME "dac"
#define NUM_SPI_MINORS 256
#define TAG "TLV5616: "

#define MAX_SPEED_HZ 20000000
#define BITS_PER_WORD 16
#define MAX_VALUE 4095

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Matthew Knight");
MODULE_DESCRIPTION("12-bit SPI DAC Driver");
MODULE_VERSION("0.1");

static struct of_device_id tlv5616_dt_ids[] = {
    {.compatible = "microchip,tlv5616"},
    {},
};

MODULE_DEVICE_TABLE(of, tlv5616_dt_ids);

// module variables
static int major_number;
static struct class* tlv5616_class;
static DECLARE_BITMAP(minors, NUM_SPI_MINORS);
static LIST_HEAD(device_list);
static DEFINE_MUTEX(device_list_lock);

// file operations interface
static int tlv5616_open(struct inode*, struct file*);
static int tlv5616_release(struct inode*, struct file*);
static ssize_t tlv5616_write(struct file*, char const*, size_t, loff_t*);

static const struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = tlv5616_open,
    .write = tlv5616_write,
    .release = tlv5616_release,
};

// spi stuff
struct device_data {
    dev_t dev;
    int ref_count;
    spinlock_t spi_lock;
    struct list_head head;
    struct spi_device* spi;
};

int tlv5616_probe(struct spi_device* spi);
int tlv5616_remove(struct spi_device* spi);

static struct spi_driver tlv5616_spi_driver = {
    .driver =
        {
            .name = DEVICE_NAME,
	    .owner = THIS_MODULE,
            .of_match_table = tlv5616_dt_ids,
        },
    .probe = tlv5616_probe,
    .remove = tlv5616_remove,
};

// function implementations
static int __init tlv5616_init(void) {
    int status;

    major_number = register_chrdev(0, DEVICE_NAME, &fops);
    if (major_number < 0) {
        printk(KERN_WARNING TAG "failed to register major number\n");
        return major_number;
    }

    tlv5616_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(tlv5616_class)) {
        unregister_chrdev(major_number, DEVICE_NAME);
        printk(KERN_WARNING TAG "failed to create device class\n");
        return PTR_ERR(tlv5616_class);
    }

    status = spi_register_driver(&tlv5616_spi_driver);
    if (status < 0) {
        class_destroy(tlv5616_class);
        unregister_chrdev(major_number, DEVICE_NAME);
        printk(KERN_WARNING TAG "failed to register spi driver\n");
        return status;
    }

    printk(KERN_INFO TAG "successfully initialized\n");
    return 0;
}

static void __exit tlv5616_exit(void) {
    class_destroy(tlv5616_class);
    unregister_chrdev(major_number, DEVICE_NAME);
    spi_unregister_driver(&tlv5616_spi_driver);
    printk(KERN_INFO TAG "exiting\n");
}

int tlv5616_probe(struct spi_device* spi) {
    struct device_data* entry;
    unsigned int minor;
    int status;

    entry = kmalloc(sizeof(struct device_data), GFP_KERNEL);
    if (!entry)
        return -ENOMEM;

    // initialize entry fields
    entry->ref_count = 0;
    entry->spi = spi;
    spin_lock_init(&entry->spi_lock);
    INIT_LIST_HEAD(&entry->head);

    // initialize tlv5616 specifics
    spi->max_speed_hz = MAX_SPEED_HZ;
    spi->bits_per_word = BITS_PER_WORD;
    spi->mode = SPI_MODE_0;

    mutex_lock(&device_list_lock);
    minor = find_first_zero_bit(minors, NUM_SPI_MINORS);
    if (minor < NUM_SPI_MINORS) {
        struct device* dev;
	printk(KERN_INFO TAG "major_number: %d\n", major_number);	
        entry->dev = MKDEV(major_number, minor);
        dev = device_create(tlv5616_class, &spi->dev, entry->dev, entry,
                            CLASS_NAME "-%d", minor);
        status = PTR_ERR_OR_ZERO(dev);
    } else {
        printk(KERN_WARNING TAG "no available minor numbers\n");
        status = -ENODEV;
    }

    if (status == 0) {
        set_bit(minor, minors);
        list_add(&entry->head, &device_list);
        spi_set_drvdata(spi, entry);
        printk(KERN_INFO TAG "added device to the list\n");
    } else {
        printk(KERN_WARNING TAG "failed to add device to the list\n");
        kfree(entry);
    }

    mutex_unlock(&device_list_lock);

    return status;
}

int tlv5616_remove(struct spi_device* spi) {
    struct device_data* entry = spi_get_drvdata(spi);

    // make sure ops on existing fds can abort cleanly
    spin_lock_irq(&entry->spi_lock);
    entry->spi = NULL;
    spin_unlock_irq(&entry->spi_lock);

    // prevent new opens
    mutex_lock(&device_list_lock);
    list_del(&entry->head);
    device_destroy(tlv5616_class, entry->dev);
    clear_bit(MINOR(entry->dev), minors);
    if (entry->ref_count == 0)
        kfree(entry);
    mutex_unlock(&device_list_lock);

    printk(KERN_INFO TAG "remove() called\n");
    return 0;
}

static int tlv5616_open(struct inode* inode, struct file* file) {
    struct device_data* entry;
    int status = -ENXIO;

    mutex_lock(&device_list_lock);
    list_for_each_entry(entry, &device_list, head) {
        if (entry->dev == inode->i_rdev) {
            status = 0;
            break;
        }
    }

    if (status == 0) {
        entry->ref_count++;
        file->private_data = entry;
        //stream_open(inode, file);
        printk(KERN_INFO TAG "opened device\n");
    } else {
        printk(KERN_WARNING TAG "nothing for minor %d\n", iminor(inode));
    }

    mutex_unlock(&device_list_lock);
    return status;
}

static int tlv5616_release(struct inode* inode, struct file* file) {
    struct device_data* entry;

    mutex_lock(&device_list_lock);
    entry = file->private_data;
    file->private_data = NULL;

    if (--entry->ref_count == 0) {
        int do_free;
        printk(KERN_INFO TAG "ref_count is zero\n");
        spin_lock_irq(&entry->spi_lock);
        do_free = (entry->spi == NULL);
        spin_unlock_irq(&entry->spi_lock);

        if (do_free) {
            printk(KERN_INFO TAG "freeing entry\n");
            kfree(entry);
        }
    } else {
        printk(KERN_INFO TAG "ref_count is %d\n", entry->ref_count);
    }

    mutex_unlock(&device_list_lock);
    return 0;
}

static ssize_t tlv5616_write(struct file* file, const char* buffer, size_t len,
                             loff_t* offset) {
    struct device_data* entry;
    int status;
    unsigned int value_buf;
    uint16_t value;
    char* end;

    char* kern_buf = kmalloc(len, GFP_KERNEL);
    status = copy_from_user(kern_buf, buffer, len);
    if (status < 0) {
	    kfree(kern_buf);
	    return status;
	}

    // find last newline and set it to be the null terminator
    for (end = kern_buf + len - 1; end != kern_buf; --end) {
	    if (*end == '\n') {
		    *end = '\0';
		    break;
	    }
    }


	status = kstrtouint(kern_buf, 10, &value_buf);
	if (status < 0) {
		printk(KERN_WARNING TAG "error converting value\n");
		kfree(kern_buf);
		return status;
	}

	if (value_buf > MAX_VALUE) {
		printk(KERN_WARNING TAG "value too large\n");
		kfree(kern_buf);
		return -ERANGE;
	}

	value = value_buf;

    mutex_lock(&device_list_lock);
    entry = file->private_data;
    status = spi_write(entry->spi, &value, sizeof(value));
    mutex_unlock(&device_list_lock);

    if (status < 0)
	    printk(KERN_WARNING TAG "error writing spi\n");

    kfree(kern_buf);
    printk(KERN_INFO TAG "tlv5616_write() called to write %zu bytes\n", len);
    return status == 0 ? len : status;
}

module_init(tlv5616_init);
module_exit(tlv5616_exit);
