obj-m+=src/tlv5616.o
KERNELDIR=/lib/modules/$(shell uname -r)/build/

all:
	make -C $(KERNELDIR) M=$(PWD) modules
clean:
	make -C $(KERNELDIR) M=$(PWD) clean
